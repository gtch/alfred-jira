module.exports = {
  module: {
    rules: [
      {
        test: /\.(plist|txt)$/i,
        use: 'raw-loader'
      },
      {
        test: /\.(js|ts)?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: [
              '@babel/preset-typescript',
              '@babel/preset-env'
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-object-assign',
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/transform-runtime'
            ]
          }
        }
      }
    ]
  }
};
