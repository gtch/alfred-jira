import * as JSZip from 'jszip';
import infoPlist from '../../../workflow/info.plist'
import workflowZip from './workflow.txt'
import {v4 as uuidv4} from 'uuid';

export interface WorkflowConfig {
  projects: string[];
  quickSearch: boolean;
  jiraUrl: string;
}

export interface ArchiveResult {
  zipBase64: string;
  size: number;
}

export async function buildArchive({jiraUrl, projects, quickSearch}: WorkflowConfig): Promise<ArchiveResult> {
  console.log(`Generating workflow for jiraUrl ${jiraUrl} quickSearch ${quickSearch} with ${projects.length} projects ${projects}`);

  const archive = await JSZip.loadAsync(workflowZip, { base64: true });

  // Generate a random UUID so that Alfred doesn't cache items between workflow updates
  const uuidPrefix = uuidv4().substr(0, 24);

  // Update the info.plist file to include the customisations
  const updatedPlist = (infoPlist as String).replace(/\{\{(.*?)\}\}/g, (match, keyword) => {
    switch (keyword) {
      case "CONNECTIONS":
        return quickSearch ? generateConnections(uuidPrefix, projects) : '';
      case "OBJECTS":
        return quickSearch ? generateObjects(uuidPrefix, projects) : '';
      case "UIDATA":
        return quickSearch ? generateUiData(uuidPrefix, projects) : '';
      case "JIRA_URL":
        return jiraUrl;
      case "JIRA_PROJECTS":
        return projects.toString();
      default:
        return match;
    }
  })

  archive.file("info.plist", updatedPlist);

  return await archive.generateAsync({ type: "nodebuffer", compression: "DEFLATE", compressionOptions: { level: 9 }}).then(buffer => {
    const zipBase64 = buffer.toString('base64');
    return ({
      zipBase64: zipBase64,
      size: buffer.length
    } as ArchiveResult);
  });
}

function generateConnections(uuidPrefix: string, projects: string[]): string {
  return projects.reduce((acc, project, index) => `${acc}
\t\t<key>${uuidPrefix}${uuidPad(index)}</key>
\t\t<array>
\t\t\t<dict>
\t\t\t\t<key>destinationuid</key>
\t\t\t\t<string>17B6E719-88BD-4BED-85DE-5C8E7C16E137</string>
\t\t\t\t<key>modifiers</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>modifiersubtext</key>
\t\t\t\t<string></string>
\t\t\t\t<key>vitoclose</key>
\t\t\t\t<false/>
\t\t\t</dict>
\t\t</array>`, '');
}

function generateObjects(uuidPrefix: string, projects: string[]): string {
  return projects.reduce((acc, project, index) => `${acc}
\t\t<dict>
\t\t\t<key>config</key>
\t\t\t<dict>
\t\t\t\t<key>alfredfiltersresults</key>
\t\t\t\t<false/>
\t\t\t\t<key>alfredfiltersresultsmatchmode</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>argumenttreatemptyqueryasnil</key>
\t\t\t\t<true/>
\t\t\t\t<key>argumenttrimmode</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>argumenttype</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>escaping</key>
\t\t\t\t<integer>102</integer>
\t\t\t\t<key>keyword</key>
\t\t\t\t<string>${project.toLowerCase()}</string>
\t\t\t\t<key>queuedelaycustom</key>
\t\t\t\t<integer>3</integer>
\t\t\t\t<key>queuedelayimmediatelyinitially</key>
\t\t\t\t<true/>
\t\t\t\t<key>queuedelaymode</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>queuemode</key>
\t\t\t\t<integer>1</integer>
\t\t\t\t<key>runningsubtext</key>
\t\t\t\t<string>Searching Jira...</string>
\t\t\t\t<key>script</key>
\t\t\t\t<string>python alfred-jira.py "{query}" "${project}"</string>
\t\t\t\t<key>scriptargtype</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>scriptfile</key>
\t\t\t\t<string></string>
\t\t\t\t<key>subtext</key>
\t\t\t\t<string></string>
\t\t\t\t<key>title</key>
\t\t\t\t<string>Jira project ${project}</string>
\t\t\t\t<key>type</key>
\t\t\t\t<integer>0</integer>
\t\t\t\t<key>withspace</key>
\t\t\t\t<false/>
\t\t\t</dict>
\t\t\t<key>type</key>
\t\t\t<string>alfred.workflow.input.scriptfilter</string>
\t\t\t<key>uid</key>
\t\t\t<string>${uuidPrefix}${uuidPad(index)}</string>
\t\t\t<key>version</key>
\t\t\t<integer>3</integer>
\t\t</dict>`, '');
}

function generateUiData(uuidPrefix: string, projects: string[]): string {
  return projects.reduce((acc, project, index) => `${acc}
\t\t<key>${uuidPrefix}${uuidPad(index)}</key>
\t\t<dict>
\t\t\t<key>note</key>
\t\t\t<string>Search project ${project}</string>
\t\t\t<key>xpos</key>
\t\t\t<integer>50</integer>
\t\t\t<key>ypos</key>
\t\t\t<integer>${200 + 150 * index}</integer>
\t\t</dict>
`, '');
}

function uuidPad(index: number) {
  return ('000000000000'+index).slice(-12);
}
