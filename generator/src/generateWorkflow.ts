import {ALBEvent, ALBResult, Context} from 'aws-lambda';
import {buildArchive} from "./util/buildArchive";

exports.handler = async (event: ALBEvent, context: Context): Promise<ALBResult> => {
  const { jiraUrl, projects, quicksearch } = event.queryStringParameters as QueryParams;

  if (!jiraUrl) {
    return error(400, 'jiraUrl was not provided');
  }

  if (!projects) {
    return error(400, 'projects was not provided');
  }

  if (!quicksearch) {
    return error(400, 'quicksearch was not provided');
  }

  const projectList = projects.split(',');

  if (projectList.reduce((acc, project) => project.length > acc ? project.length : acc, 0) > 10) {
    return error(400, 'project keys must be no longer than 10 characters');
  }

  const quickSearchBoolean = quicksearch === 'true';

  const zip = await buildArchive({
    jiraUrl,
    projects: projectList,
    quickSearch: quickSearchBoolean
  });

  const zipBase64 = zip.zipBase64;

  return {
    statusCode: 200,
    statusDescription: 'OK',
    headers: {
      'Cache-Control': 'no-cache no-store max-age=0 must-revalidate',
      'Content-Disposition': 'attachment; filename="AlfredJira.alfredworkflow"',
      'Content-Type': 'application/octet-stream'
    },
    body: zipBase64,
    isBase64Encoded: true
  };
};

interface QueryParams {
  jiraUrl?: string;
  projects?: string;
  quicksearch?: string;
}

function error(code: number, message: string) {
  return {
    statusCode: code,
    statusDescription: message,
    body: `Error: ${message}`,
    isBase64Encoded: false
  }
}
