---
layout: page
title: 'Jira Cloud search workflow for Alfred'
permalink: /
---

`alfred-jira` allows you to search Jira Cloud directly in the Alfred app for macOS. It generates an
Alfred workflow customised for your Jira Cloud site.

Before you can search Jira from Alfred you need to generate a workflow customised for your Jira Cloud instance.
There is a Forge app available for configuring this workflow, and you can also generate a workflow by filling in
the form below. 

## Generate an Alfred workflow now 

If you don't have the Forge app installed you can [generate a workflow here](/generate).

## How to generate an Alfred workflow with the Forge app

If you have the Forge app installed look for the 'Configure Alfred' menu item on an issue. Click on that menu item in any Jira issue.

![Screenshot of 'Configure Alfred' menu item](images/configure-alfred-menu-item.png)

A popup will appear that lets you configure the workflow:

* Choose one or more projects that you want to search from Alfred. Click 'All projects' to add all projects to the workflow.
* Choose whether to enable quick search (recommended!). When quick search is enabled you can search for an issue simply by typing the project name, no need to type `jira` first.

![Screenshot of 'Configure Alfred' popup](images/configure-alfred-popup.png)

Now click 'Generate Alfred Workflow' to continue.

A link will be displayed for your custom workflow. Click the link to download it.

![Screenshot of 'Generate Workflow' popup](images/generate-workflow-popup.png)

An `AlfredJira.alfredworkflow` file should be downloaded to your Mac. Double-click it to open it in Alfred. You should
now see the workflow installation screen:

![Screenshot of Alfred workflow installation](images/install-alfred-workflow.png)

The URL of your Jira Cloud site will be filled in. You need to fill in the `JIRA_USERNAME` and `JIRA_PASSWORD` fields
with your username and password — or an API token, which is even better. Using an API token is better because then your
Jira password won't be stored within Alfred. If you don't have an API token, go to 
https://id.atlassian.com/manage-profile/security/api-tokens and generate one. Enter the value of the token into the
`JIRA_PASSWORD` field instead of your password.

## Searching Jira with Alfred

There are a few ways to search for issues:
* Type an issue key _(if quick search enabled)_
* Type a project key plus a search query _(if quick search enabled)_
* Type in `jira` plus a search query

Type in an issue key for one of the projects you selected when generating the workflow, if you had quick search enabled.
Alfred will show you that issue, plus any issues linked to the one you typed. eg:
```
SOFT-5
```

![Screenshot of searching Jira with Alfred](images/alfred-jira-issue-search.png)

You can also type in a project key then a simple text query afterwards, eg to search for all the issues with the word
'turtles' on project CS:
```
CS turtles
```

The above methods only work if quick search is enabled. Quick search adds each project key to the Alfred workflow keywords
so that you only need to type the project key to start searching. There should be no reason to disable quick search
unless one of the project keys conflicts with another keyword you use in Alfred.

The final method of searching is to type `jira` then a simple text query afterwards to search all projects for
the query. This method does not require quick search to be enabled. eg:
```
jira turtles
```


---

## License

`alfred-jira` is free and open-source, licensed with the [MIT license](/license). Source code is available
at the Bitbucket respository:

* https://bitbucket.org/gtch/alfred-jira

The generated workflows include the `alfred-workflow` library which also uses the MIT license:

* https://github.com/deanishe/alfred-workflow

The generated workflows include the Jira logo mark which is trademark of Atlassian and is not MIT licensed.
Atlassian permits "fair use" of this trademark, and `alfred-jira` uses it according to the guidelines at:

* https://www.atlassian.com/legal/trademark
