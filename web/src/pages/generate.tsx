import * as React from 'react'

import '@atlaskit/reduced-ui-pack'

import Page from '../components/Page'
import Container from '../components/Container'
import IndexLayout from '../layouts'
import { Link } from 'gatsby'


const GeneratePage = () => (
  <IndexLayout>
    <Page>
      <Container>
        <h1>Jira Cloud workflow for Alfred</h1>
        <p>
          This <code>alfred-jira</code> app generates a workflow for the MacOS app <a href="https://alfredapp.com/">Alfred</a>
          that lets you search Jira Cloud issues quickly. It is designed to be used from within you Jira Cloud account
          with a Forge app, however if you don't have that installed you can also generate a workflow manually here.
        </p>

        <p>Read more on <Link to="/">the homepage</Link>.</p>

        <h2>Generate a workflow</h2>

        <form method="GET" action="https://alfred-jira.netlify.app/.netlify/functions/generateWorkflow">
          <div className="ak-field-group">
            <label htmlFor="jiraUrl">Jira Cloud URL</label>
            <input
              type="url"
              className="ak-field-url"
              id="jiraUrl"
              name="jiraUrl"
              placeholder="eg: https://example.atlassian.net"
              required
            />
          </div>
          <div className="ak-field-group">
            <label htmlFor="projects">Allow Alfred to search these projects</label>
            <label><em>(Type project keys separated by commas, no spaces)</em></label>
            <input
              type="text"
              className="ak-field-text"
              id="projects"
              name="projects"
              placeholder="eg: AA,BBB,CCCC"
              pattern="[A-Za-z]+(,[A-Za-z]+)*"
              required
            />
          </div>
          <fieldset className="ak-field-group">
            <legend>
              <span>Quick search</span>
            </legend>
            <div className="ak-field-radio">
              <input
                type="radio"
                name="quicksearch"
                id="quicksearchtrue"
                value="true"
                defaultChecked
              />
              <label htmlFor="quicksearchtrue">
                Enable quick search
              </label>
            </div>
            <div className="ak-field-radio">
              <input
                type="radio"
                name="quicksearch"
                id="quicksearchfalse"
                value="false"
              />
              <label htmlFor="quicksearchfalse">
                Disable quick search
              </label>
            </div>
            <div>Quick search lets you search issue keys in Alfred without typing <code>jira</code> first.</div>
          </fieldset>
          <div className="ak-field-group">
            <button className="ak-button ak-button__appearance-primary" type="submit">
              Generate Alfred Workflow
            </button>
          </div>
        </form>


      </Container>
    </Page>
  </IndexLayout>
)

export default GeneratePage
