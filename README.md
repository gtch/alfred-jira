# Alfred search for Jira Cloud

`alfred-jira` allows you to search Jira Cloud directly in the Alfred app for macOS. It generates an
Alfred workflow customised for your Jira Cloud site.

Main website: https://alfred-jira.netlify.app/ 

![Screenshot of searching Jira with Alfred](web/src/content/images/alfred-jira-issue-search.png)


## How to build and install `alfred-jira`

There are four components to this app:

* **forge:** an Atlassian Forge app that provides configuration UI inside Jira.
* **generator:** a Netlify function / lambda which generates an Alfred workflow file
* **web:** a Gatsby static website which contains user documentation to deploy to Netlify
* **workflow:** the Python source code for the Alfred workflow  

If you don't have access to Forge then you can generate a workflow manually at https://alfred-jira.netlify.app/generate

### Build the Netlify components

The repository is preconfigured with a `netlify.toml` file so that if you deploy it to Netlify the build should occur 
automatically without you needing to configure anything. 

However these components do not require Netlify specifically. If you wish to build the Netlify components locally, or
wish to run them on another host, simply run a standard `yarn` build:

```
yarn install
yarn build
```

This will:

* Compress the contents of the `/workflow` directory into a `workflow.txt` file inside the generator source.
* Build the generator into a single Netlify function / lambda called `generateWorkflow.js`.
* Build the Gatsby site in `/web` into a static HTML website.

### Build the Forge app

Before building the Forge app you will need to [install the Forge CLI as documented by Atlassian](https://developer.atlassian.com/platform/forge/getting-started/).

Assuming you wish to deploy straight into a production environment then run:

```
forge deploy -e production
```

Then specify the location of the generator function you deployed to Netlify (or somewhere else), for example:

```
forge variables:set -e production DOWNLOAD_URL "https://alfred-jira.netlify.app/.netlify/functions/generateWorkflow"
```
(Make sure to update the URL with your own deployed URL)

Finally install the Forge app into your Jira instance:

```
forge install -e production
```

Note that the Forge app provides a user interface to let you configure your Alfred workflow, 
but is not required to run the workflow. The generated Alfred workflow uses Jira APIs directly 
so the app does not need to remain installed once you have generated a workflow. 

---

## License

`alfred-jira` is free and open-source, licensed with the [MIT license](/license). Source code is available
at the Bitbucket respository:

* https://bitbucket.org/gtch/alfred-jira

The workflows generated by `alfred-jira` include the `alfred-workflow` library which also uses the MIT license:

* https://github.com/deanishe/alfred-workflow

The generated workflows include the Jira logo mark which is trademark of Atlassian and is not MIT licensed.
Atlassian permits "fair use" of this trademark, and `alfred-jira` uses it according to the guidelines at:

* https://www.atlassian.com/legal/trademark
