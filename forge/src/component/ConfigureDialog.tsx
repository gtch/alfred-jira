import ForgeUI, {
  Button,
  Form,
  Fragment,
  ModalDialog,
  Option,
  Radio,
  RadioGroup,
  Select,
  Text,
  useProductContext,
  useState
} from "@forge/ui";
import {fetchProjects, fetchServerInfo} from "../api/jiraApi";
import {DOWNLOAD_URL} from "../config";

interface DownloadParams {
  newSelectedProjects: string[];
  newQuickSearch: boolean;
}

export const ConfigureDialog = () => {
  const [ isOpen, setOpen ] = useState(true);
  const [ projects ] = useState(async () => fetchProjects());
  const [ quickSearch, setQuickSearch] = useState(true);
  const [ serverInfo ] = useState(async () => fetchServerInfo());
  const [ selectedProjects, setSelectedProjects ] = useState(() => {
    const projectMap = new Map<string, boolean>();
    const context = useProductContext();
    if (context && context.platformContext && context.platformContext["projectKey"]) {
      projectMap.set(context.platformContext["projectKey"], true);
    }
    return projectMap;
  })
  const [ downloadLink, setDownloadLink ] = useState<string>(undefined)
  const [ downloadProjectCount, setDownloadProjectCount ] = useState<number>(undefined)

  const downloadUrlIsConfigured = !!DOWNLOAD_URL;

  if (!isOpen) {
    return null;
  }

  async function downloadWorkflow({ newSelectedProjects, newQuickSearch }: DownloadParams): Promise<void> {
    const jiraUrl = encodeURIComponent(serverInfo && serverInfo.baseUrl);
    console.log(`downloadWorkflow() called for ${jiraUrl} with projects ${newSelectedProjects} and quicksearch ${newQuickSearch}`);
    setDownloadProjectCount(newSelectedProjects.length);
    setDownloadLink(`${DOWNLOAD_URL}?jiraUrl=${jiraUrl}&projects=${newSelectedProjects.reduce((acc, p) => acc + ',' + p)}&quicksearch=${!!newQuickSearch}`);
  }

  async function enableAllProjects() {
    if (projects) {
      // Construct a map containing every project key set to 'true'
      const p = projects.reduce(
        (selected, project) => selected.set(project.key, true),
        new Map<string, boolean>()
      );
      setSelectedProjects(p);
    }
  }

  return (
    <ModalDialog header="Configure Alfred" onClose={() => setOpen(false)} closeButtonText="Close">
      {!downloadUrlIsConfigured && (
        <Fragment>
          <Text>Before you can configure Alfred please configure the download URL in Forge to wherever the generator code
            is deployed. The standard deployment is at Netlify:</Text>
          <Text>`forge variables:set DOWNLOAD_URL "https://alfred-jira.netlify.app/.netlify/functions/generateWorkflow"`</Text>
        </Fragment>
      )}
      {downloadUrlIsConfigured && !downloadLink && (
        <Fragment>
          <Text>Configure the [Alfred app](https://www.alfredapp.com/) to search Jira issues directly from macOS. [Read more...](https://alfred-jira.netlify.app/)</Text>
          <Form onSubmit={downloadWorkflow} submitButtonText="Generate Alfred Workflow">
          <Select label="Allow Alfred to search these projects" name="newSelectedProjects" placeholder="Select projects..." isMulti={true}>
          {projects && projects.map(project => {
            return (
              <Option
                label={`${project.name} (${project.key})`}
                defaultSelected={selectedProjects && selectedProjects.get && !!selectedProjects.get(project.key)}
                value={project.key} />
            )
          })}
          </Select>
          <Button onClick={enableAllProjects} text="All projects" />
          <RadioGroup label="Quick search" description="Search by issue key" name="newQuickSearch" isRequired={true}>
          <Radio value="true" defaultChecked={quickSearch} label="Enable quick search" />
          <Radio value="false" defaultChecked={!quickSearch} label="Disable quick search" />
          </RadioGroup>
          <Text>Quick search lets you search issue keys in Alfred without typing `jira` first.</Text>
          </Form>
        </Fragment>
      )}
      {downloadUrlIsConfigured && downloadLink && (
        <Fragment>
          <Text>
            Workflow generated for {downloadProjectCount} project{downloadProjectCount !== 1 && 's'}:
            {'\n\n'}
            **[Download Alfred Workflow]({downloadLink})**
          </Text>
        </Fragment>
      )}
    </ModalDialog>
  );
};
