export interface Project {
  id: string;
  key: string;
  name: string;
  projectTypeKey: string;
}

export interface ProjectSearchResult {
  maxResults: number;
  total: number;
  startAt: number;
  isLast: boolean;
  values: Project[];
}
