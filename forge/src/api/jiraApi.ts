import api from "@forge/api";
import {Project, ProjectSearchResult} from "../model/Project";

export async function fetchProjects(): Promise<Project[]> {
  const response = await api.asApp().requestJira("/rest/api/3/project/search");
  const searchResult = await response.json() as ProjectSearchResult;
  return searchResult.values.sort((a, b) => a.name.localeCompare(b.name));
}

export async function fetchServerInfo(): Promise<ServerInfo> {
  const response = await api.asApp().requestJira("/rest/api/3/serverInfo");
  return response.json();
}

export interface ServerInfo {
  baseUrl: string;
  version: string;
  versionNumbers: number[];
  deploymentType: string;
  buildNumber: number;
  buildDate: string;
  serverTime: string;
  scmInfo: string;
  serverTitle: "Jira";
  defaultLocale: {
    locale: string
  }
}
