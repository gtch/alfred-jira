import ForgeUI, {IssueAction, render} from '@forge/ui';
import {ConfigureDialog} from "./component/ConfigureDialog";

export const run = render(
  <IssueAction>
    <ConfigureDialog/>
  </IssueAction>
);
