# encoding: utf-8

import base64
import os
import sys
import logging
import re
from workflow import Workflow3, ICON_WEB, web, ICON_WARNING


class Config:
    wf = None  # type: Workflow3

    def __init__(self, wf, jira_url, jira_username, jira_password, jira_projects):
        self.wf = wf
        self.jira_url = jira_url
        self.jira_username = jira_username
        self.jira_password = jira_password
        self.jira_projects = jira_projects


def load_config(wf):
    jira_url = os.getenv('JIRA_URL')
    jira_username = os.getenv('JIRA_USERNAME')
    jira_password = os.getenv('JIRA_PASSWORD')
    jira_projects = os.getenv('JIRA_PROJECTS')

    if not jira_url:
        error(wf, 'Jira URL is not configured', 'Please set the JIRA_URL environment variable on the Alfred Jira workflow')
        return None

    elif not jira_username:
        error(wf, 'Jira username is not configured', 'Please set the JIRA_USERNAME environment variable on the Alfred Jira workflow')
        return None

    elif not jira_password:
        error(wf, 'Jira password is not configured', 'Please set the JIRA_PASSWORD environment variable on the Alfred Jira workflow')
        return None

    elif not jira_projects:
        error(wf, 'Jira projects not configured', 'Please set the JIRA_PROJECTS environment variable on the Alfred Jira workflow')
        return None

    else:
        return Config(wf, jira_url, jira_username, jira_password, jira_projects)


def error(wf, primary, secondary):
    wf.add_item(title='Error: ' + primary,
                subtitle=secondary,
                valid=False,
                icon=ICON_WEB)


def request_headers(config):
    """
    Generates the appropriate headers for a web request to Jira.
    :type config: Config
    """
    authorization_header = 'Basic ' + base64.b64encode(config.jira_username + ':' + config.jira_password)
    headers = {
        'accept-encoding': 'application/json',
        'authorization': authorization_header
    }
    return headers


def show_issue_history(config, project):
    """
    Uses the Jira API /rest/api/3/issue/picker to load the issue history
    :type config: Config
    :type project: string
    """
    logging.info('Showing issue history from ' + config.jira_url)

    url = config.jira_url + '/rest/api/3/issue/picker'
    params = dict(
        currentProjectId=project
    )
    r = web.get(url, params=params, headers=request_headers(config))

    # throw an error if request failed
    # Workflow will catch this and show it to the user
    r.raise_for_status()

    result = r.json()
    sections = result['sections']
    issues = sections[0]['issues']

    # Loop through the returned posts and add an item for each to the list of results for Alfred
    for issue in issues:
        key = issue['key']
        if project:
            autocomplete = key[len(project):]
        else:
            autocomplete = key

        config.wf.add_item(title=issue['summaryText'],
                           subtitle='Jira ' + key,
                           arg=config.jira_url + '/browse/' + key,
                           autocomplete=autocomplete,
                           valid=True,
                           icon=ICON_WEB)


def show_issue_and_related(config, issue_key):
    """
    Uses the Jira API /rest/api/3/issue/{key} to load a single issue and its related issues
    :type config: Config
    :type issue_key: string
    """
    logging.info('Showing issue ' + issue_key + ' from ' + config.jira_url)

    url = config.jira_url + '/rest/api/3/issue/' + issue_key
    params = dict(
        fields="summary,issuetype,issuelinks"
    )
    r = web.get(url, params=params, headers=request_headers(config))

    if r.status_code == 404:
        # This issue was not found
        config.wf.add_item(title='Issue ' + issue_key + ' not found',
                           subtitle='Jira ' + issue_key,
                           valid=False,
                           icon=ICON_WARNING)
        return

    # throw an error if request failed
    # Workflow will catch this and show it to the user
    r.raise_for_status()

    result = r.json()
    key = result['key']
    fields = result['fields']
    issuetype = fields['issuetype']
    issuelinks = fields['issuelinks']

    # Add the issue itself to the search results
    config.wf.add_item(title=fields['summary'],
                       subtitle='Jira ' + key,
                       arg=config.jira_url + '/browse/' + key,
                       valid=True,
                       icon=ICON_WEB)

    # Loop through the related issues and add an item for each to the list of results for Alfred
    for issuelink in issuelinks:
        if issuelink.has_key('inwardIssue'):
            linked_issue = issuelink['inwardIssue']
            relationship = issuelink['type']['outward']

        elif issuelink.has_key('outwardIssue'):
            linked_issue = issuelink['outwardIssue']
            relationship = issuelink['type']['inward']

        else:
            # Links should always have inwardIssue or outwardIssue, but if not just ignore it
            continue

        linked_key = linked_issue['key']
        config.wf.add_item(title=linked_issue['fields']['summary'],
                           subtitle='Jira ' + linked_key + ' ' + relationship + ' ' + key,
                           arg=config.jira_url + '/browse/' + linked_key,
                           valid=True,
                           icon=ICON_WEB)


def show_issue_search(config, project, query):
    """
    Uses the Jira API /rest/api/3/search to search for issues matching the query
    :type config: Config
    :type project: string
    :type query: string
    """
    if project:
        logging.info('Searching ' + config.jira_url + ' project ' + project + ' for query "' + query + '"')
    else:
        logging.info('Searching ' + config.jira_url + ' for query "' + query + '"')

    url = config.jira_url + '/rest/api/3/search'
    if project:
        # Search one project only
        jql = 'project=' + project + ' AND text~"' + query + '"'
    else:
        # Search all projects enabled in the config
        jql = 'project IN (' + config.jira_projects + ') AND text ~ "' + query + '"'

    params = dict(
        fields='summary,issuetype',
        jql=jql
    )
    r = web.get(url, params=params, headers=request_headers(config))

    # throw an error if request failed
    # Workflow will catch this and show it to the user
    r.raise_for_status()

    result = r.json()
    issues = result['issues']

    if not issues:
        # No matching issues wer not found
        config.wf.add_item(title='No Jira issues found for "' + query + '"',
                           subtitle='Jira',
                           valid=False,
                           icon=ICON_WARNING)

    # Loop through the returned posts and add an item for each to the list of results for Alfred
    for issue in issues:
        key = issue['key']
        if project:
            autocomplete = key[len(project):]
        else:
            autocomplete = key

        config.wf.add_item(title=issue['fields']['summary'],
                           subtitle='Jira ' + key,
                           arg=config.jira_url + '/browse/' + key,
                           autocomplete=autocomplete,
                           valid=True,
                           icon=ICON_WEB)


def main(wf):
    """
    :type wf: Workflow3
    """
    config = load_config(wf)
    if config:
        # Get query from Alfred
        if len(wf.args) > 0:
            query = wf.args[0].strip()  # type: str
        else:
            query = None

        if len(wf.args) > 1:
            project = wf.args[1]
        else:
            project = None

        # Decide what kind of query to perform
        if project and query:
            # Does the look like an issue key, or is it more like a search query?
            if query == '-':
                # This query is incomplete so don't show anything yet
                config.wf.add_item(title='Jira project ' + project,
                                   subtitle='Keep typing issue key...',
                                   valid=False,
                                   icon=ICON_WEB)

            elif re.search(r"^-\d{1,10}$", query):
                # This query looks like an issue key
                issue_key = project + query
                show_issue_and_related(config, issue_key)

            else:
                # This query doesn't look like an issue key, so do a search
                show_issue_search(config, project, query)

        elif not project and query:
            # Search all projects
            show_issue_search(config, None, query)

        else:
            # There is no query to show so browse the issue history
            show_issue_history(config, project)

    # Send the results to Alfred as JSON. If there was invalid config an error will be sent back instead.
    wf.send_feedback()


if __name__ == u"__main__":
    wf = Workflow3()
    sys.exit(wf.run(main))
